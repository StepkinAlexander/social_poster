# -*- coding: utf-8 -*-
import requests
import odnoklassniki
from requests_toolbelt.multipart.encoder import MultipartEncoder

from django.shortcuts import render

from .forms import UploadFileForm


app_id = '1250214400'
app_public_key = 'CBADMFILEBABABABA'
app_secret_key = '40D158ECD8CB231A6E39D682'
access_token = 'tkn1IMuKANfEFHdNfszvea15ySwC5ehMSaRvvL4xuckzkvY6mXtmGv4XrjrRe8xJI5ulF5'
group_id = '53376785252554'
api = odnoklassniki.Odnoklassniki(app_public_key, app_secret_key, access_token)

'''
пул из id который удаляет из него завершённые закачки
и время от времени удаляет записи что осталиь в пуле,
иначе плодится очень много сущностей'''
from datetime import datetime
from time import sleep
class VideoUploadPoolRequests(object):
    pool_expired_settings = 30  # 30sec
    pool_requests = []

    def __init__(self, expired_seconds=30):
        self.pool_expired_settings = expired_seconds

    def push_pool(self, value):
        self.pool_requests.append({
                'timestamp': datetime.now(),
                'value': str(value),
            })

    def pop_pool(self, value, handler=None):
        length = len(self.pool_requests)
        for i in range(length):
            index = length-i-1
            item = self.pool_requests[index]
            if item['value'] == value:
                del self.pool_requests[index]
                if handler:
                    handler(item(value))

    def inspect_pool(self, handler):
        length = len(self.pool_requests)
        for i in range(length):
            index = length-i-1
            item = self.pool_requests[index]
            expired = (datetime.now() - item['timestamp']).seconds > self.pool_expired_settings
            if expired:
                handler(item['value'])
                del self.pool_requests[index]

'''
TODO
нужна серьёзная доработка пула
'''
pool = VideoUploadPoolRequests(30)

def get_action():
    url_obj = api.video.getUploadUrl(gid=group_id, file_name='загружаемое видео', file_size=0)
    video_id = url_obj.get('video_id')
    url = url_obj.get('upload_url')
    pool.push_pool(video_id)
    return (url, video_id)

def handle_uploaded_file(action, f):
    multipart_data = MultipartEncoder(fields={'video': (f.name, f, f.content_type)})
    resp = requests.post(action, data=multipart_data, headers={'Content-Type': multipart_data.content_type})
    return resp

def video_accept(video_id):
    result = None
    try:
        result=api.video.update(vid=video_id)
    except ValueError, ex:
        if ex.message=='No JSON object could be decoded':
            return True
        return False
    except:
        return False

    '''согласно манулу, штатное завершение загрузки возвращает '', однако
    простая отправка данных формы(html) возвращает <retval>1</retval>,
    а python возвращает ValueError'''
    return True if result=='' else result

def delete_video(video_id):
    result=None
    try:
        result=api.video.delete(vid=video_id)
    except:
        '''ситуация похожа на завершение загрузки, апи часто возвращает исключения
        по поводу и без'''
        pass
    return True if result=='' else result

def upload(request):
    action = None
    video_id = None
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        action = request.POST.get('action')
        video_id = request.POST.get('video_id')
        hdl = None
        try:
            hdl = handle_uploaded_file(action, request.FILES['video'])
            if hdl.status_code==200:
                if video_accept(video_id):
                    pool.pop_pool(video_id)
                    return render( request, 'ok/result.html', {'status': 'ok'})
            delete_video(video_id)
            return render( request, 'ok/result.html', {'status': 'upload failed<br>%s' % hdl.content})
        except Exception, ex:
            delete_video(video_id)
            if hdl:
                return render(request, 'ok/result.html', {'status': 'upload failed<br>%s' % hdl.content})
            else:
                return render(request, 'ok/result.html', {'status': 'upload unhandled error'})
    else:
        form = UploadFileForm()
        (action, video_id) = get_action()
    pool.inspect_pool(delete_video)
    print pool.pool_requests
    return render( request, 'ok/upload.html', { 'form': form, 'action': action, 'video_id': video_id })
