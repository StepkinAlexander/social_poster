venv=python

migrate:
	$(venv)/bin/python manage.py makemigrations && \
	$(venv)/bin/python manage.py migrate && \
	$(venv)/bin/python manage.py migrate --run-syncdb

run:
	$(venv)/bin/python manage.py runserver 0.0.0.0:8000 --settings=project.local_settings
